$(document).ready(function (){

	//плавный переход по якорям 
 	$(".nav li").click(function(){
        var link = $(this).find("a").attr("href");
        $("html, body").animate({scrollTop : $(link).offset().top},400);
		
        setTimeout(function(){location.hash = link;},100);		
		return false;
	}); 


    //счётчик - кнопка плюс
    
     $(".counter .plus").click(function(){
          var input = $(this).siblings('input');
          var value = Number(input.val());
          input.val(value + 1);
    });
    
    //счётчик - кнопка минус
    
    $(".counter .minus").click(function(){
          var input = $(this).siblings('input');
          var value = Number(input.val());
          if(value > 1) input.val(value - 1);
    });

   


    $('.items__carousel').slick({
		  infinite: false,
		  speed: 500,
		  slidesToShow: 4,
		  slidesToScroll: 4,
		  responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
              }
            },
		    {
		      breakpoint: 1024,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2,
		        infinite: true,
		        dots: true
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		        slidesToScroll: 2
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        slidesToShow: 1,
		        slidesToScroll: 1
		      }
		    }
		    // You can unslick at a given breakpoint now by adding:
		    // settings: "unslick"
		    // instead of a settings object
		  ]
		});


    $(".popup .popup__close").click(function(){
          $(".popup__wrapper").hide();
    });

    $(".search_btn").click(function(){
        $(".popup__wrapper, .popup__search").show();
        return false;
    });

    $("nav .popup__close").click(function(){
          $("nav").removeClass("opened");
    });

     $(".menu_btn").click(function(){
          $("nav").addClass("opened");
    });

     $(".catalog__menu li.hassub").click(function(){
          $(this).toggleClass("active");
    });

     

     $( ".catalog__filters select" ).selectmenu();
}); //конец ready